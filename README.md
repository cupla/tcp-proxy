# tcp-proxy

Proxies and firewalls incoming TCP connections. Configuration with YAML file and docker compose.

FREE TO USE.

Copyright (c) Cupla Software Teemu Lätti

https://hub.docker.com/r/cuplasoftware/tcp-proxy  
https://gitlab.com/cupla/tcp-proxy

## tcp-proxy.yml

This example configures to listen port 5432 and proxies the connection to postgres container. Only connections from remote IP address "1.2.3.4" are allowed.

```yaml
postgres:
    listen: 5432
    connect: postgres:5432
    allow:
        - 1.2.3.4
```

Multiple services can be configured in the YAML file. The name identifies the service for automatic update and is used in logging.

Supported parameters:

| Parameter | Description                                                                                             |
| :-------- | :------------------------------------------------------------------------------------------------------ |
| listen    | Bind address and port (or only port) to listen for incoming connections.                                |
| connect   | Outgoing connection made for each incoming connection.                                                  |
| allow     | Allowed remote addresses. If none given, nothing is allowed. If "\*" is given (quoted), all is allowed. |

## docker-compose.yml

This example configures tcp-proxy as a docker container. Here tcp-proxy firewalls incoming connections so that postgres can only be reached from allowed remote IP addresses (preventing password attacks). External ports must be specified also here to allow the container to be reached. Postgres specifies no ports since docker handles network bridge between containers. The YAML configuration file can be edited locally since it has been mapped to the container. If you only change allowed addresses, changes will be taken automatically, but if you make other changes, you need to restart the container.

```yaml
version: "3"
services:
    tcp-proxy:
        container_name: tcp-proxy
        image: cuplasoftware/tcp-proxy
        restart: unless-stopped
        volumes:
            - $PWD/tcp-proxy.yml:/tcp-proxy.yml
        ports:
            - 5432:5432

    postgres:
        container_name: postgres
        image: postgres:latest
        restart: unless-stopped
```
