package main

import (
	"errors"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"golang.org/x/exp/slices"
	"gopkg.in/yaml.v3"
)

const (
	ConfigFileName = "tcp-proxy.yml"
)

type Config struct {
	Listen   string    // listen [host:]port
	Connect  string    // connect target
	Allow    []string  // allowed IP addresses
	modified time.Time // config file modified time
}

func main() {
	log.Println("start")

	// Read config file
	configs := loadConfig()

	// Listen each
	for name, config := range configs {
		if len(config.Listen) == 0 {
			log.Fatal("no listen for ", name)
		}
		if len(config.Connect) == 0 {
			log.Fatal("no connect for ", name)
		}
		addr := config.Listen
		if !strings.Contains(addr, ":") {
			addr = ":" + addr
		}
		listen, err := net.Listen("tcp", addr)
		if err != nil {
			log.Fatal(err)
		}
		log.Println(name, "listen", addr)

		// Listen incoming connections
		go func(name string, config *Config) {
			for {
				// Wait for new incoming connection
				con, err := listen.Accept()
				if err != nil {
					log.Println(err)
					continue
				}

				// Before connecting, check if config file has been modified
				if !config.modified.IsZero() && getConfigModified().After(config.modified) {
					// Reload this config (reloading all configs requires restart)
					configs = loadConfig()
					config = nil
					for n, c := range configs {
						if n == name {
							config = c
						}
					}
					if config == nil {
						log.Println(name, "removed")
						return
					}
					log.Println(name, "modified")
				}

				go onConnect(con, name, config)
			}
		}(name, config)
	}

	// Wait for termination
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs

	log.Println("exit")
}

func loadConfig() map[string]*Config {
	// Read yaml file
	data, err := os.ReadFile(ConfigFileName)
	if err != nil {
		log.Fatal(err)
	}

	// Parse yaml
	configs := make(map[string]*Config)
	err = yaml.Unmarshal([]byte(data), &configs)
	if err != nil {
		log.Fatal(err)
	}

	// Store modified time
	modified := getConfigModified()
	for _, config := range configs {
		config.modified = modified
	}

	return configs
}

func getConfigModified() time.Time {
	fileInfo, err := os.Stat(ConfigFileName)
	if err != nil {
		return time.Time{}
	}
	return fileInfo.ModTime()
}

func onConnect(from net.Conn, name string, config *Config) {
	defer from.Close()

	// Remote IP
	remote := from.RemoteAddr()
	if remote == nil {
		log.Println(name, "denied unknown ip")
		return
	}
	remoteIP := remote.String()
	i := strings.LastIndex(remoteIP, ":")
	if i >= 0 {
		remoteIP = remoteIP[0:i]
	}
	if len(remoteIP) == 0 {
		log.Println(name, "denied unknown ip")
		return
	}

	// Check that allowed
	if slices.Contains(config.Allow, "*") {
		// All is allowed
	} else if !slices.Contains(config.Allow, remoteIP) {
		log.Println(name, "denied", remoteIP)
		return
	}

	log.Println(name, "connect", remoteIP)

	// Connect to target
	to, err := net.Dial("tcp", config.Connect)
	if err != nil {
		log.Println(name, err)
		return
	}

	// Data transfer to both directions until closed
	go copy(from, to, name)
	copy(to, from, name)

	log.Println(name, "close", remoteIP)
}

func copy(from, to net.Conn, name string) {
	_, err := io.Copy(to, from)
	if err != nil && !errors.Is(err, io.EOF) && !errors.Is(err, net.ErrClosed) {
		log.Println(name, err)
	}
	to.Close()
}
