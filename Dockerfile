FROM --platform=$BUILDPLATFORM golang:alpine AS build
ARG TARGETOS
ARG TARGETARCH
WORKDIR /build
ADD * .
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -o tcp-proxy .

FROM alpine
COPY --from=build /build/tcp-proxy /tcp-proxy
ENTRYPOINT ["/tcp-proxy"]
